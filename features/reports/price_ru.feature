@price-ru-only
Feature: Generating reports for price.ru campaigns
  Background:
    Given I am site user Bob
    And now is "2014-02-02 13:00:00"

  @critical
  Scenario: Prices and links to shop offers of that product on price.ru
    Given I create campaign for price.ru with price from txt file with rows
      | name           |
      | Apple iPhone 6 |
    And this campaign is for region "Москва"
    And this campaign has settings
      | sort_by_price |
      | 1             |
    And row "Apple iPhone 6" has price.ru model with url "https://price.ru/mobilnye-telefony/apple-iphone-6-128gb/" and model id "667644"
    And this price parsed and search is finished
    And I set this campaign report format to XLSX and report columns
      | merged product name |
      | offers              |
      | link_to_offers      |
    And I create report for this campaign
    And this report started
    And product "Apple iPhone 6" parsed with one offer on price.ru
    And this report completed
    Then this report should have XLSX file with 1 sheet
    And there should be data (including links) on sheet "Москва" of this report file
      | Название       | Ссылка на предложения                                                        | Магазин 1   | Цена 1                                                                        |
      | Apple iPhone 6 | [Показать предложения](https://price.ru/model/667644/offers/?sort=price-asc) | Apple-4g.ru | [23 990](https://price.ru/model/667644/offers/?sort=price-asc&shop_id=969518) |