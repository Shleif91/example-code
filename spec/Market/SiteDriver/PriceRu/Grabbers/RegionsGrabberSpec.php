<?php

declare(strict_types=1);

namespace spec\Market\SiteDriver\PriceRu\Grabbers;

use Market\SiteDriver\PriceRu\Grabbers\RegionsGrabber;
use PhpSpec\ObjectBehavior;

/** @mixin RegionsGrabber **/
class RegionsGrabberSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(RegionsGrabber::class);
    }

    function it_can_get_towns_from_product_card()
    {
        $content = file_get_contents(__DIR__ . '/../json/regions_search_char_A.json');

        $regions = [
            ['id' => 13, 'name' => 'Архангельск'],
            ['id' => 33, 'name' => 'Альметьевск'],
        ];

        $this->getRegionsFromJson($content)->shouldBe($regions);
    }
}