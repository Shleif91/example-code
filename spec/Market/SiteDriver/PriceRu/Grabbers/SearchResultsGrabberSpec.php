<?php

declare(strict_types=1);

namespace spec\Market\SiteDriver\PriceRu\Grabbers;

use Market\SiteDriver\PriceRu\Grabbers\SearchResultsGrabber;
use PhpSpec\ObjectBehavior;
use Market\SiteDriver\PriceRu\FoundPriceRuModel;

/** @mixin SearchResultsGrabber **/
class SearchResultsGrabberSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(SearchResultsGrabber::class);
    }

    function it_is_can_get_searched_models()
    {
        $data = $this->fetchModels(file_get_contents(__DIR__ . '/../html/search_results_with_two_models.html'));
        $data->shouldBeLike([
            new FoundPriceRuModel(
                'Смартфон Apple iPhone 6s 64Gb',
                'https://price.ru/mobilnye-telefony/apple-iphone-6s-64gb/',
                819565
            ),
            new FoundPriceRuModel(
                'Смартфон Apple iPhone 6s 32GB',
                'https://price.ru/mobilnye-telefony/apple-iphone-6s-32gb/',
                988628
            ),
        ]);
    }

    function it_is_can_distinguish_product_page_and_get_model()
    {
        $data = $this->fetchModels(file_get_contents(__DIR__ . '/../html/product_page.html'));
        $data->shouldBeLike([
            new FoundPriceRuModel(
                'Смартфон Apple iPhone 6 16Gb',
                'https://price.ru/mobilnye-telefony/apple-iphone-6-16gb/',
                654054
            )
        ]);
    }
}