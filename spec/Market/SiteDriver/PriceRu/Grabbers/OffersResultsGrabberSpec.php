<?php

declare(strict_types=1);

namespace spec\Market\SiteDriver\PriceRu\Grabbers;

use Market\Doctrine\Entity\Shop;
use Market\SiteDriver\PriceRu\PriceRuProductOffer;
use Market\SiteDriver\PriceRu\Grabbers\OffersResultsGrabber;
use PhpSpec\ObjectBehavior;

/** @mixin OffersResultsGrabber **/
class OffersResultsGrabberSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(OffersResultsGrabber::class);
    }

    function it_can_get_product_from_json()
    {
        $content = file_get_contents(__DIR__ . '/../json/single_offer_iphone_6.json');

        $this->fetchOffersFromJson($content)->shouldBeLike([
            new PriceRuProductOffer(
                'GSM24',
                'http://mob-mir-shop.ru/',
                969272,
                14790,
                true,
                false,
                1,
                .0,
                true
            )
        ]);
    }

    function it_can_get_product_from_json_without_delivery()
    {
        $content = file_get_contents(__DIR__ . '/../json/single_offer_iphone_6_without_delivery.json');

        $this->fetchOffersFromJson($content)->shouldBeLike([
            new PriceRuProductOffer(
                'GSM24',
                'http://mob-mir-shop.ru/',
                969272,
                14790,
                true,
                false,
                1,
                .0,
                true
            )
        ]);
    }

    function it_can_get_many_products_from_json()
    {
        $content = file_get_contents(__DIR__ . '/../json/offers_iphone_6.json');

        $this->fetchOffersFromJson($content)->shouldBeLike([
            new PriceRuProductOffer(
                'GSM24',
                'http://mob-mir-shop.ru/',
                969272,
                14790,
                true,
                false,
                1,
                350.0,
                true
            ),
            new PriceRuProductOffer(
                'KupiPhone',
                'http://kupi-phone.ru/',
                976821,
                10790,
                true,
                false,
                2,
                null,
                false
            )
        ]);
    }

    function it_can_round_prices_from_offers()
    {
        $content = file_get_contents(__DIR__ . '/../json/offers_iphone_6_with_not_round_prices.json');

        $this->fetchOffersFromJson($content)->shouldBeLike([
            new PriceRuProductOffer(
                'GSM24',
                'http://mob-mir-shop.ru/',
                969272,
                14790,
                true,
                false,
                1,
                350.0,
                true
            ),
            new PriceRuProductOffer(
                'KupiPhone',
                'http://kupi-phone.ru/',
                976821,
                10791,
                true,
                false,
                2,
                null,
                false
            )
        ]);
    }
}