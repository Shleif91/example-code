<?php

declare(strict_types=1);

namespace spec\Market\SiteDriver\PriceRu;

use Market\SiteDriver\PriceRu\DeliveryPriceConverter;
use PhpSpec\ObjectBehavior;

/** @mixin DeliveryPriceConverter */
class DeliveryPriceConverterSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(DeliveryPriceConverter::class);
    }

    public function it_is_convert_values()
    {
        $this->getPrice('asd1000 1.3 р.')->shouldBe(1.3);
        $this->getPrice('1 000.3 руб.')->shouldBe(1000.3);
        $this->getPrice('1000000 рублей')->shouldBe(1000000.0);
        $this->getPrice('3,000.5 р')->shouldBe(3000.5);
        $this->getPrice('бесплатно')->shouldBe(.0);
        $this->getPrice('Бесплатно')->shouldBe(.0);
        $this->getPrice('Курьером')->shouldBe(null);
    }
}
