<?php

namespace spec\Market\SiteDriver\PriceRu;

use Market\SiteDriver\PriceRu\PriceRuLinksGenerator;
use PhpSpec\ObjectBehavior;

/** @mixin PriceRuLinksGenerator **/
class PriceRuLinksGeneratorSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(PriceRuLinksGenerator::class);
    }

    function it_can_generate_link_to_all_offers_in_json()
    {
        $this->generateLinkGetOffersInJson(849571, 1)
            ->shouldReturn('https://price.ru/services/offers?model_id=849571&endpoint=page.model-offer-list&region_id=1&limit=100');
    }

    function it_can_generate_link_to_all_offers_in_json_with_town_and_limit()
    {
        $this->generateLinkGetOffersInJson(849571, 3, false, 20)
            ->shouldReturn('https://price.ru/services/offers?model_id=849571&endpoint=page.model-offer-list&region_id=3&limit=20');
    }
}