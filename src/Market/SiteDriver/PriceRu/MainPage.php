<?php

declare(strict_types=1);

namespace Market\SiteDriver\PriceRu;

interface MainPage
{
    public function search(string $searchTerm): PriceRuSearchResult;
}
