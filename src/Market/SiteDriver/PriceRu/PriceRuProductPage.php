<?php

namespace Market\SiteDriver\PriceRu;

use Market\BrowserDriver\BrowserSession;
use Market\SiteDriver\PriceRu\Grabbers\OffersResultsGrabber;
use Market\BrowserDriver\Request;

class PriceRuProductPage implements ProductPage
{
    private $browserSession;
    private $priceRuModelId;
    private $regionId;
    private $sortByPrice;

    public function __construct(BrowserSession $browserSession, int $priceRuModelId, int $regionId, bool $sortByPrice)
    {
        $this->browserSession = $browserSession;
        $this->priceRuModelId = $priceRuModelId;
        $this->sortByPrice = $sortByPrice;
        $this->regionId = $regionId;
    }

    public function getOffers(?int $minShopRating): array
    {
        $url = PriceRuLinksGenerator::generateLinkGetOffersInJson($this->priceRuModelId, $this->regionId, $this->sortByPrice);
        $response = $this->browserSession->request(Request::get($url));

        if ($response->status !== 200) {
            throw new \RuntimeException(sprintf('Got non-OK response code from price.ru during search: %s', $response->status));
        }

        $foundModels = (new OffersResultsGrabber())->fetchOffersFromJson($response->content);

        return $foundModels;
    }
}