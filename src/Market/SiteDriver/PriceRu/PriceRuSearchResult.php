<?php

declare(strict_types=1);

namespace Market\SiteDriver\PriceRu;

class PriceRuSearchResult
{
    private $foundModels;

    public function __construct(FoundPriceRuModel ...$foundModels)
    {
        $this->foundModels = $foundModels;
    }

    public function hasOneModel(): bool
    {
        return count($this->foundModels) === 1;
    }

    public function noModelsFound(): bool
    {
        return count($this->foundModels) === 0;
    }

    public function multipleModelsFound(): bool
    {
        return count($this->foundModels) > 1;
    }

    public function getTheOnlyModel(): FoundPriceRuModel
    {
        if (!$this->hasOneModel()) {
            throw new \Exception('Multiple models where found, but the only model is requested');
        }

        return $this->foundModels[0];
    }

    /** @return FoundPriceRuModel[] */
    public function getAllModels(): array
    {
        return $this->foundModels;
    }

}
