<?php

declare(strict_types=1);

namespace Market\SiteDriver\PriceRu;

class DeliveryPriceConverter
{
    public static function getPrice(string $value): ?float
    {
        preg_match('/((\d+)\ ?\,?(\d{3})?\.?(\d+)?.?(р\.?|руб\.?|рублей))/', $value, $res);

        if (!$res) {
            if (mb_strtolower($value) == 'бесплатно') {
                return .0;
            } else {
                return null;
            }
        }

        $price = preg_replace('/[^.\d]/', '', $res[0]);

        if ($price[mb_strlen($price) - 1] == '.') {
            mb_substr($price, 0, -1);
        }

        return (float)$price;
    }
}
