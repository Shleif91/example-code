<?php

namespace Market\SiteDriver\PriceRu;

class PriceRuLinksGenerator
{
    const DOMEN_NAME = 'https://price.ru';
    const SEARCH_URL = self::DOMEN_NAME . '/search/?query=';
    const JSON_URL = self::DOMEN_NAME . '/services/offers?';

    public static function generateLinkGetOffersInJson(int $id, int $regionId, bool $sortByPrice, ?int $limit): string
    {
        $parameters = [
            'model_id' => $id,
            'endpoint' => 'page.model-offer-list',
            'region_id' => $regionId,
            'limit' => (!is_null($limit) && $limit > 0) ? $limit : 100,
        ];

        if ($sortByPrice) {
            $parameters['sort'] = 'price-asc';
        }

        return self::JSON_URL . http_build_query($parameters);
    }

    public static function generateLinkToProductOffers(int $productId): string
    {
        return self::DOMEN_NAME . '/model/' . $productId . '/offers/';
    }
}