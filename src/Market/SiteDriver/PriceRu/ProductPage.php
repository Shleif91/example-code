<?php

declare(strict_types=1);

namespace Market\SiteDriver\PriceRu;

interface ProductPage
{
    public function getOffers(?int $minShopRating): array;
}
