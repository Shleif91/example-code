<?php

declare(strict_types=1);

namespace Market\SiteDriver\PriceRu\Grabbers;

class RegionsGrabber
{
    public function getRegionsFromJson(string $jsonContent): array
    {
        $data = json_decode($jsonContent, true);
        $regions = [];

        foreach ($data['regions']['items'] as $region) {
            $regions[] = [
                'id' => $region['id'],
                'name' => $region['name'],
            ];
        }

        return $regions;
    }
}
