<?php

declare(strict_types=1);

namespace Market\SiteDriver\PriceRu\Grabbers;

use Market\SiteDriver\PriceRu\DeliveryPriceConverter;
use Market\SiteDriver\PriceRu\PriceRuProductOffer;

class OffersResultsGrabber
{
    public function fetchOffersFromJson(string $content): array
    {
        $offers = [];
        $data = json_decode($content, true);

        foreach ($data['items'] as $key => $item) {
            $offers[] = new PriceRuProductOffer(
                $item['shopName'],
                $item['shop']['url'],
                (int)$item['shop']['id'],
                (int)round($item['price_unformatted']),
                $item['availability'] == 'в наличии' ? true : false,
                $item['pickup'] ? true : false,
                $key + 1,
                isset($item['delivery']) ? DeliveryPriceConverter::getPrice($item['delivery']) : null,
                isset($item['warranty']) && mb_strtolower($item['warranty']) == 'производителя' ? true : false
            );
        }

        return $offers;
    }
}