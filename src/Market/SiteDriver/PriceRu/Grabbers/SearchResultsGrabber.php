<?php

declare(strict_types = 1);

namespace Market\SiteDriver\PriceRu\Grabbers;

use Market\SiteDriver\PriceRu\PriceRuLinksGenerator;
use Symfony\Component\DomCrawler\Crawler;
use Market\SiteDriver\PriceRu\FoundPriceRuModel;

class SearchResultsGrabber
{
    /** @return FoundPriceRuModel[] */
    public function fetchModels(string $pageContent): array
    {
        $crawler = new Crawler($pageContent);
        $models = [];

        // If this is a empty page
        if ($crawler->filter('.b-search-emptytext > p')->count()) {
            return $models;
        };

        // If this is a page with many products
        $crawler->filter('article.b-list-viewlist__item_model')->each(function (Crawler $elem) use (&$models){
            $title = $this->getTitleFromManyProductsPage($elem);
            $url = $this->getUrlFromSearchResultNode($elem);
            $id = (int)$elem->attr('id');
            $models[] = new FoundPriceRuModel($title, $url, $id);
        });

        // If this is a product page
        if (!$models) {
            $title = $this->getTitleFromProductPage($crawler);
            $url = $this->getProductUrlFromProductPage($crawler);
            $id = $this->getIdFromProductPage($crawler);
            $models[] = new FoundPriceRuModel($title, $url, $id);
        }

        return $models;
    }

    /** @return string */
    private function getTitleFromProductPage(Crawler $crawler): string
    {
        $title = '';
        $crawler->filter('.modelcard__title > h1 > span')->each(function (Crawler $elem) use (&$title) {
            $title .= trim($elem->text()) . ' ';
        });
        $title = rtrim($title);
        $title = mb_strtoupper(mb_substr($title, 0, 1)) . mb_substr($title, 1);

        return $title;
    }

    private function getUrlFromSearchResultNode(Crawler $crawler): string
    {
        $selector = 'h3.b-list-viewtile__item-title > a';
        $link = $crawler->filter($selector)->attr('href');
        $clearLink = explode('?', $link)[0];
        $url = PriceRuLinksGenerator::DOMEN_NAME . $clearLink;

        return $url;
    }

    private function getIdFromProductPage(Crawler $crawler): int
    {
        $pattern = '#model/(?<model_id>\d+)/offers#u';
        $content = $crawler->html();

        if (!preg_match($pattern, $content, $matches)) {
            throw new \Exception('Model ID cannot be fetched from product page.');
        }

        return (int)$matches['model_id'];
    }

    /** @return string */
    private function getTitleFromManyProductsPage(Crawler $crawler): string
    {
        $str = $crawler->filter('h3.b-list-viewtile__item-title > a')->text();
        $title = preg_replace('/\s+/', ' ', $str);

        return $title;
    }

    private function getProductUrlFromProductPage(Crawler $crawler): string
    {
        $node = $crawler->filter('link[rel="canonical"]');
        if ($node->count() === 0) {
            throw new \Exception('Model link node not found.');
        }

        return $node->first()->attr('href');
    }
}