<?php

declare(strict_types=1);

namespace Market\SiteDriver\PriceRu;

use Market\BrowserDriver\BrowserSession;
use Market\BrowserDriver\Request;
use Market\SiteDriver\PriceRu\Grabbers\SearchResultsGrabber;

class PriceRuMainPage implements MainPage
{
    private $browserSession;

    public function __construct(BrowserSession $browserSession)
    {
        $this->browserSession = $browserSession;
    }

    public function search(string $searchTerm): PriceRuSearchResult
    {
        $response = $this->browserSession->request(Request::get(PriceRuLinksGenerator::SEARCH_URL . urlencode($searchTerm)));

        if ($response->status !== 200) {
            throw new \RuntimeException(sprintf('Got non-OK response code from price.ru during search: %s', $response->status));
        }

        $foundModels = (new SearchResultsGrabber())->fetchModels($response->content);

        return new PriceRuSearchResult(...$foundModels);
    }
}
