<?php

declare(strict_types=1);

namespace Market\SiteDriver\PriceRu;

class PriceRuProductOffer
{
    private $shopName;
    private $shopUrl;
    private $shopId;
    private $price;
    private $inStock;
    private $pickup;
    private $position;
    private $delivery;
    private $warranty;

    public function __construct(
        string $shopName,
        string $shopUrl,
        int $shopId,
        int $price,
        bool $inStock,
        bool $pickup,
        int $position,
        ?float $delivery = null,
        bool $warranty = null
    ) {
        $this->shopName = $shopName;
        $this->shopUrl = $shopUrl;
        $this->shopId = $shopId;
        $this->price = $price;
        $this->inStock = $inStock;
        $this->pickup = $pickup;
        $this->position = $position;
        $this->delivery = $delivery;
        $this->warranty = $warranty;
    }

    public function getShopName(): string
    {
        return $this->shopName;
    }

    public function getShopUrl(): string
    {
        return $this->shopUrl;
    }

    public function getShopId(): int
    {
        return $this->shopId;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getDelivery(): ?float
    {
        return $this->delivery;
    }

    public function getWarranty(): bool
    {
        return $this->warranty;
    }

    public function toArray(): array
    {
        return [
            'price' => $this->price,
            'delivery_price' => $this->delivery,
            'in_stock' => $this->inStock,
            'pickup' => $this->pickup,
            'producer_warranty' => $this->warranty,
        ];
    }
}
