<?php

declare(strict_types=1);

namespace Market\SiteDriver\PriceRu;

class FoundPriceRuModel
{
    private $name;
    private $modelId;
    private $webUrl;

    public function __construct(string $name, string $webUrl, int $modelId)
    {
        $this->name = $name;
        $this->modelId = $modelId;
        $this->webUrl = $webUrl;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getWebUrl(): string
    {
        return $this->webUrl;
    }

    public function getModelId(): int
    {
        return $this->modelId;
    }
}
