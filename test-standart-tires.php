<?php

declare(strict_types = 1);

require 'app/autoload.php';


$loader = require_once __DIR__.'/app/bootstrap.php.cache';
require_once __DIR__.'/app/AppKernel.php';

$kernel = new AppKernel('dev', true);

$kernel->boot();

// New file with 'standard' column
$standard_handle = fopen(__DIR__ . "/files/standard/tires_price_with_standard.csv", "w+");

if (($handle = fopen(__DIR__ . "/files/standard/tires-utf-8.txt", "r")) !== false) {
    while (($data = fgetcsv($handle, 1000, "\n")) !== false) {
        $num = count($data);
        $str = '';
        for ($c = 0; $c < $num; $c++) {
            $str = $data[$c];
        }

        // Split a line into parts
        $params = preg_split("/(;|\t)/", $str);
        // Get Width/Height and Radius
        $arrWidthHeightRadius = preg_split("/(R|\-)/", $params[6]);
        // Clear index Z from Width/Height
        $arrWidthHeightRadius[0] = str_replace('Z', '', $arrWidthHeightRadius[0]);
        // Collect the name from the parts
        $name = "$params[4] $params[5] $arrWidthHeightRadius[0] R$arrWidthHeightRadius[1] $params[10]$params[11]";
        fwrite($standard_handle, $str . '\t' . $name . PHP_EOL);
    }
    fclose($handle);
}

fclose($standard_handle);
